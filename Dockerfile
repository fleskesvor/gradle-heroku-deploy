FROM gradle:5.3

USER root
RUN curl https://cli-assets.heroku.com/install.sh | bash

USER gradle
RUN heroku plugins:install java
